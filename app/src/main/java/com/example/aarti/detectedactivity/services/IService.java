package com.example.aarti.detectedactivity.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.example.aarti.detectedactivity.R;
import com.example.aarti.detectedactivity.util.Constants;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

/**
 * Author :     Lalit Umbarkar
 * Created On : 3/6/17
 * Project:     DetectedActivity
 */

public class IService extends IntentService {

    private static String TAG = "IService";
    private static Integer NOTIFY_ID = 1245;

    public IService() {
        super("IService");
    }

    public IService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (ActivityRecognitionResult.hasResult(intent)) {
            Log.i(TAG, "Intent found Recognition Result");
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities(result.getMostProbableActivity());
        }
    }

    private void handleDetectedActivities(DetectedActivity mostProbableActivity) {
        String activity = Constants.getActivityString(this, mostProbableActivity.getType());
        Log.i(TAG, "Found Activity as " + activity);
        createNotification("You might be " + activity);
    }

    private void createNotification(String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentText(message);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("Activity Update");
        NotificationManagerCompat.from(this).notify(NOTIFY_ID, builder.build());
    }


}
