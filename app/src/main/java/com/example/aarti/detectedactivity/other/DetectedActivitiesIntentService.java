package com.example.aarti.detectedactivity.other;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.aarti.detectedactivity.util.Constants;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;

/**
 * Created by Aarti on 5/29/2017.
 */
public class DetectedActivitiesIntentService extends IntentService {

    protected static final String TAG = "DetectedActivitiesIS";

    Handler mHandler;

    /**
     * This constructor is required, and calls the super IntentService(String)
     * constructor with the name for a worker thread.
     */
    public DetectedActivitiesIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
        mHandler = new Handler();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Handles incoming intents.
     * @param intent The Intent is provided (inside a PendingIntent) when requestActivityUpdates()
     *               is called.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        Intent localIntent = new Intent(Constants.BROADCAST_ACTION);

        // Get the list of the probable activities associated with the current state of the
        // device. Each activity is associated with a confidence level, which is an int between
        // 0 and 100.
        ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();

        // Log each activity.
        Log.i(TAG, "activities detected");
        for (DetectedActivity da: detectedActivities) {

            if(da.getType() == DetectedActivity.ON_FOOT && da.getConfidence()>=20)
                mHandler.post(new DisplayToast(this,"Walking"));
            else
                mHandler.post(new DisplayToast(this, "Not Walking"));


            Log.i(TAG, Constants.getActivityString(
                            getApplicationContext(),
                            da.getType()) + " " + da.getConfidence() + "%"
            );
        }

        // Broadcast the list of detected activities.
        localIntent.putExtra(Constants.ACTIVITY_EXTRA, detectedActivities);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }


    /*protected static final String TAG = "DetectedActivitiesIS";
    DetectedActivity da;

    public DetectedActivitiesIntentService() {      //calls the super IntentService(String) constructor with the name for a worker thread.
          super(TAG);     // Use the TAG to name the worker thread.
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {      //Handles incoming intents.
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        Intent localIntent = new Intent(Constants.BROADCAST_ACTION);

       // if(da.getType() == DetectedActivity.ON_FOOT && da.getConfidence() >= 20){   //Broadcast walking activity.
            localIntent.putExtra(Constants.ACTIVITY_EXTRA, DetectedActivity.ON_FOOT);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
   }*/
}
