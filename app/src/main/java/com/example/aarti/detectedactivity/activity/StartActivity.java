package com.example.aarti.detectedactivity.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.aarti.detectedactivity.R;
import com.example.aarti.detectedactivity.services.IService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;

/**
 * Author :     Lalit Umbarkar
 * Created On : 3/6/17
 * Project:     DetectedActivity
 */

public class StartActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public int log_line_num = 0;
    public GoogleApiClient gpc;
    public TextView logger_tv;
    Button trigger_btn;
    Button request_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        // Get UI Elements
        logger_tv = (TextView) findViewById(R.id.logger_tv);
        request_btn = (Button) findViewById(R.id.request_btn);
        trigger_btn = (Button) findViewById(R.id.connect_btn);

        // Register Listeners
        request_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, IService.class);
                PendingIntent pendingIntent = PendingIntent.getService(StartActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(gpc, 200, pendingIntent);
                logger_tv.append(++log_line_num + " : Request for Update to ActivityRecognition\n");
            }
        });
        trigger_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Init Google Client
                gpc = new GoogleApiClient.Builder(StartActivity.this)
                        .addConnectionCallbacks(StartActivity.this)
                        .addOnConnectionFailedListener(StartActivity.this)
                        .addApi(ActivityRecognition.API)
                        .build();
                gpc.connect();
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        logger_tv.append(++log_line_num + " : Connected To GPC\n");
    }

    @Override
    public void onConnectionSuspended(int i) {
        logger_tv.append(++log_line_num + " : Connection Suspended To GPC\n");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }


}
