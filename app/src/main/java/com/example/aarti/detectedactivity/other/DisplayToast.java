package com.example.aarti.detectedactivity.other;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Aarti on 5/31/2017.
 */

class DisplayToast implements Runnable {
    private final Context mContext;
    String mText;

    DisplayToast(Context mContext, String text) {
        this.mContext = mContext;
        mText = text;
    }

    public void run(){
        Toast.makeText(mContext, mText, Toast.LENGTH_SHORT).show();
    }
}
